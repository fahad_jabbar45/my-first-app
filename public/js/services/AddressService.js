angular.module('AddressService', []).factory('AddressService', ['$http', function($http) {


    return{
        createAddress: function (address,success,error)
        {
            $http.post('/createAddress_',student,{}).

                success(function (data) {
                    success(data);
                }).
                error(function (data) {
                    error(data);

                });
        },


        getAddress : function (id,success,error)
        {
            $http.get('/getAddress_/'+id,{}).

                success(function (data) {

                    success(data);

                }).
                error(function (data) {
                    error(data);

                });

        },

        showAddress : function (id,success,error)
        {


            $http.get('/showAddress_/'+id,{}).

                success(function (data) {

                    success(data);

                }).
                error(function (data) {
                    error(data);

                });

        },

        deleteAddress: function (id,success,error)
        {
            $http.delete('/deleteAddress_/'+id,{}).

                success(function (data) {
                    success(data);
                }).
                error(function (data) {
                    error(data);
                });



        },

        updateAddress: function (address,success,error)
        {

            var id=address._id;

            console.log(id);

             $http.put('/updateAddress_',address).

                  success(function (data) {
                    success(data);
                  }).
            error(function (data) {
                error(data);

            });


    },
        getAllAddresses:function (success,error)
        {

            $http.get('/address_', {}).

                success(function (data) {

                    success(data);
                }).
                error(function (data) {
                    error(data);

                });

        }



    }
}]);