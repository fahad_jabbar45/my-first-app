angular.module('StudentService', []).factory('StudentService', ['$http', function($http) {


    return{
        createStudent: function (student,success,error)
        {
            $http.post('/createStudent',student,{}).

                success(function (data) {
                    success(data);
                }).
                error(function (data) {
                    error(data);

                });
        },


        getStudent : function (id,success,error)
        {
            $http.get('/getStudent/'+id,{}).

                success(function (data) {

                    success(data);

                }).
                error(function (data) {
                    error(data);

                });

        },

        deleteStudent: function (id,success,error)
        {
            $http.delete('/deleteStudent/'+id,{}).

                success(function (data) {
                    success(data);
                }).
                error(function (data) {
                    error(data);
                });



        },

        updateStudent: function (student,success,error)
        {

            var id=student._id;
            console.log(id);

             $http.put('/updateStudent_/'+id,student).

                  success(function (data) {
                    success(data);
                  }).
            error(function (data) {
                error(data);

            });


    },
        getAllStudents:function (success,error)
        {

            $http.get('/student_', {}).

                success(function (data) {

                    success(data);
                }).
                error(function (data) {
                    error(data);

                });

        }



    }
}]);