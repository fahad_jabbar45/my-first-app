angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/signUp.html',
			controller: 'SignUpController'
		})

		.when('/home', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/nerds', {
			templateUrl: 'views/nerd.html',
			controller: 'NerdController'
		})

		.when('/geeks', {
			templateUrl: 'views/geek.html',
			controller: 'GeekController'	
		})
		.when('/user', {
			templateUrl: 'views/user.html',
			controller: 'UserController'
		})
		.when('/createStudent', {
			templateUrl: 'views/createStudent.html',
			controller: 'StudentController'
		})
		.when('/students',{
			templateUrl:'views/student.html',
			controller:'StudentController'
		})
		.when('/update/:id',{
			templateUrl: 'views/updateStudent.html',
			controller: 'UpdateStudentController'
		})
		.when('/addresses/:id',{
			templateUrl: 'views/address.html',
			controller: 'ShowAddressController'
		})
		.when('/updateaddress/:id',{
			templateUrl: 'views/updateAddress.html',
			controller: 'UpdateAddressController'
		})
		.when('/signUp',{
			templateUrl :'views/signUp.html' ,
			controller: 'SignUpController'
	    })
		.when('/signIn',{
			templateUrl :'views/signIN.html' ,
			controller: 'SignUpController'
		})
		.when('/materialSignIn' , {
			templateUrl:'views/materialSignIn.html',
			controller:'SignUpController'

		})

	;

	$locationProvider.html5Mode(true);

}]);