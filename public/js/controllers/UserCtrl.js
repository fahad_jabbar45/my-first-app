angular.module('UserCtrl', []).controller('UserController', function($scope) {

	$scope.tagline = 'The square root of life is pi!';

	$scope.name = '';
	$scope.email = '';
	$scope.passw1 = '';
	$scope.passw2 = '';
	$scope.users = [
		{id:1 , name:'Hege Pege', email:"Hege@gmail.com",passw1:"Hege" , passw2:"Hege" },
		{id: 2 , name:'Kim',  email:"Kim@gmail.com" ,passw1:"hege" , passw2:"Kim"},
		{id:3, name:'Sal',  email:"Sal@gmail.com" ,passw1:"Sal" , passw2:"Sal"},
		{id:4, name:'Jack', email:"Jack@gmail.com" ,passw1:"Jack" , passw2:"Jack"},
		{id:5, name:'John', email:"John@gmail.com" ,passw1:"John" , passw2:"John"},
		{id:6, name:'Peter',email:"Peter@gmail.com" ,passw1:"Peter" , passw2:"Peter"},
		{id:7, name:'Salman',  email:"Salman@gmail.com" ,passw1:"Salman" , passw2:"Salman"},
		{id:8, name:'Jackie', email:"Jackie@gmail.com" ,passw1:"Jackie" , passw2:"Jackie"},
		{id:9, name:'Johnie', email:"Johnie@gmail.com" ,passw1:"Johnie" , passw2:"Johnie"},
		{id:10, name:'Peter Gold',email:"Petergold@gmail.com" ,passw1:"Petergold" , passw2:"Petergold"}
	];


	$scope.edit = true;
	$scope.error = false;
	$scope.incomplete = false;
	$scope.hideform = true;


	$scope.deleteUser = function(user) {
		$scope.hideform = false;

		$scope.edit = false;
		var index = $scope.users.indexOf(user);
		$scope.bdays.splice(index, 1);


	};

	$scope.addUser=function  (){

		id1= $scope.users[$scope.users.length-1].id + 1 ;
		$scope.users.push({id: id1 , name : $scope.name, email : $scope.email , passw1 : $scope.passw1 , passw2:$scope.passw2 });
	}



	$scope.editUser = function() {

		$scope.hideform = false;

		$scope.edit = true;
		$scope.incomplete = true;
		$scope.name = '';
		$scope.email = '';

	};

	$scope.$watch('passw1',function() {$scope.test();});
	$scope.$watch('passw2',function() {$scope.test();});
	$scope.$watch('name', function() {$scope.test();});
	$scope.$watch('email', function() {$scope.test();});

	$scope.test = function() {
		if ($scope.passw1 !== $scope.passw2) {
			$scope.error = true;
		} else {
			$scope.error = false;
		}
		$scope.incomplete = false;
		if ($scope.edit && (
			!$scope.passw1.length || !$scope.passw2.length)) {
			$scope.incomplete = true;
		}
	};






});