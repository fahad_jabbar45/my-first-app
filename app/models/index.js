/**
 * Created by NovatoreSolutions on 17/02/16.
 */

module.exports = function(app) {

    var _                 = require('underscore');
    var fs                = require('fs');
    var path              = require('path');
    var log               = app.log;

    app.models = app.models || {};

    fs.readdirSync(__dirname).forEach(function(f) {
        if (f !== "index.js" && path.extname(f) === '.js'){
            var model = require(path.join(__dirname,f))(app);
            if (model && model.name && model.name.length && !(model.name in app.models)) {
                //log.info('loading model: ' + model.name);
                app.models[model.name] = model;
            }
        }
    });

};