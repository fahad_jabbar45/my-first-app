// grab the mongoose module

// define our nerd model
// module.exports allows us to pass this to other files when it is called
module.exports = function(app) {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
    var Model = {
	name: 'Address',
	schema: new Schema({
		country: {type: String, default: 'Pakistan'},
		statee: {type: String, default: "Lahore"},
		town: {type: String, default: "Johar Town"},
		street: {type: String, default: "6"},
		houseNumber: {type: String, default: "595"},
		student:{type:mongoose.Schema.Types.ObjectId, ref: 'Student'}
	})


     };


	 return Model;
};

