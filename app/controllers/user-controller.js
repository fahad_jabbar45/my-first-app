/**
 * Created by NovatoreSolutions on 17/02/16.
 */

var bcrypt = require('bcrypt-nodejs');

var validPassword = function(password, storedPassword) {
    return bcrypt.compareSync(password, storedPassword);
};
var generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    //return password;
};


module.exports = function(app,passport) {
    var User = require('mongoose').model('User');


    var jwt = require('jwt-simple');

    var Controller = {
        name: 'User'
    };



   // user controller

     Controller.signedInUser = function(req, res)
     {

         if (req.user)
         {
               res.send(req.user);
         }
         else
         {
            res.send({
             msg:"Not Signedin",
             status: false
                     });
         }

     }



     Controller.signOut = function(req, res) {
     req.logout();
     res.send({
     msg: "successfully logged out",
     status: true
     });
     }

     Controller.signIn = function(req, res) {
     passport.authenticate('local-login', function(err, user, info) {
     if (err) {
     app.errorHandler.handle500(err, res)
     }
     if (!user) {
     res.send(info)
     } else {
     req.logIn(user, function(err) {
     res.send(user)
     })
     }
     })(req, res)
     }

     Controller.signUp = function(req, res){
     passport.authenticate('local-signup', function (err, user, info) {

     if(!user)
     res.send(info);
     else {
         req.logIn(user, function (err) {
             res.send(user)
         })

     }
     })(req, res);

     }


    Controller.jwtSignIn = function (req,res)
    {
         User.findOne(  { email:req.body.email} ,function ( err,user){
             if (err) {

                 res.json({"status": "error", "message": "Email id is wrong", "response": ""});
                 return;

                 console.log(err);



                     }

             else
                 {
                     if (validPassword(req.body.password,user.password)){

                         var successFulSign = genToken(user);

                         res.json({"status": "Ok","statusCode":"200" , "message": "Signed In", "response": successFulSign});
                         return;


                     }
                     else {

                         res.json({"status": "error", "message": "invalid Password", "response": ""});
                         return;

                     }
                 }
                    // res.send(addresses);

             }



         )



    };





    Controller.jwtSignUp = function (req,res)
    {
        User.findOne(  { email:req.body.email} ,function ( err,user){
                if (err) {

                    console.log(err);

                    res.json({"status": "error", "message": "Connection Problem", "response": ""});
                    return;




                }

                else
                {

                    if(!user )
                    {
                        var encryptPass = generateHash(req.body.password);

                        var user = new User({
                            email: req.body.email.toLowerCase(),
                            password: encryptPass,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName

                        });

                        user.save(function(err) {
                            if (err) {
                                console.log(err);

                            }
                            else {

                                var successFulSign = genToken(user);

                                res.json({"status": "Ok","statusCode":"200" , "message": "Signed Up", "response": successFulSign});
                                return;




                            }
                        })

                    }



                    else {

                        res.json({"status": "error", "message": "Email Id alrady taken", "response": ""});
                        return;

                    }
                }
                // res.send(addresses);

            }



        )



    };




    function genToken(user) {
        var expires = expiresIn(2); // 2 days
        var token = jwt.encode({
            exp: expires
        }, 'super.super.secret.shhh');

        return {
            token: token,
            expires: expires,
            user: user
        };
    }
    function expiresIn(numDays)
    {
        var dateObj = new Date();
        return dateObj.setDate(dateObj.getDate() + numDays);
    }




    return Controller
};