/**
 * Created by NovatoreSolutions on 17/02/16.
 */


module.exports = function(app,passport) {
    var Student = require('mongoose').model('Student');
    var Address= require('mongoose').model('Address');

    var Controller = {
        name: 'Address'
    };


    Controller.createAddress = function(req,res){

     console.log(req.body);

        var address = new Address (req.body);

        address.save(function(err) {
            if (err) {
                console.log(err);

            } else {



                res.send(address);
            }
        });


    };

    Controller.getAddress=function(req,res) {
        console.log(req.params.id);
        Address.findOne(req.params.id, function (err, address) {
            if (err) {
                console.log(err);
            } else {
                res.send(address);
            }

        });
    };


    Controller.getAllAddresses=function(req,res){

        var addresses= [];

        Address.find({}, function (err,addresses){
                if (err){
                    console.log(err);
                }
                else{

                    res.send(addresses);
                }

            });

        };

    Controller.deleteAddress=function(req,res){

        Address.findByIdAndRemove(req.params.id  , function (err, address) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("Deleted");
            }


        });


    };



    Controller.updateAddress=function(req,res){

        console.log(req.body);
      Address.findOneAndUpdate(req.params.id, req.body,function (err,address){
          if (err) {
              console.log(err);
          }
          else {
              res.send("Updated");
          }


      });


    };

    Controller.showAddress=function(req,res){


        Address.find({ student : req.params.id },function ( err,addresses){
            if (err) {
                console.log(err);
            }
            else {
                res.send(addresses);
            }


        });





    };





    return Controller
};