/**
 * Created by NovatoreSolutions on 17/02/16.
 */


module.exports = function(app,passport) {
    var Student = require('mongoose').model('Student');
    var Address= require('mongoose').model('Address');
    var Controller = {
        name: 'Student'
    };


    Controller.createStudent = function(req,res){

     console.log(req.body);

        var student = new Student (req.body);

        student.save(function(err) {
            if (err) {
                console.log(err);

            } else {
                var addres = new Address ();
                addres=req.body.address;

               for (i=0;i<addres.length;i++)
               {
                   addres[i].student=student._id;
               }

                Address.create (addres, function(err) {
                    if (err) {
                        console.log(err);

                    }
                    else {

                        console.log("updated");
                    }
                });






                res.send(student);
            }
        });


    };

    Controller.getStudent=function(req,res) {
        console.log(req.params.id);
        Student.findOne(req.params.id, function (err, student) {
            if (err) {
                console.log(err);
            } else {
                res.send(student);
            }

        });
    };


    Controller.getAllStudents=function(req,res){

        var addresss= new Address () ;

       var students=[ ];

        Student.find({}, function (err,students){
                if (err){
                    console.log(err);
                }
                else{

                    res.send(students);
                }

            });

        };

    Controller.deleteStudent=function(req,res){

        Student.findByIdAndRemove(req.params.id  , function (err, student) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("Deleted");
            }


        });


    };



    Controller.updateStudent=function(req,res){
        console.log('ID'+req.params.id)
      Student.findOneAndUpdate(req.params.id, req.body,function (err,student){
          if (err) {
              console.log(err);
          }
          else {
              res.send("Updated");
          }


      });


    };



   // user controller
    /*
     Controller.signedinuser = function(req, res) {
     if (req.user) {
     res.send(req.user);
     } else {
     res.send(null);
     }
     }
     Controller.signout = function(req, res) {
     req.logout();
     res.send({
     msg: "successfully logged out",
     status: true
     });
     }
     Controller.signIn = function(req, res) {
     passport.authenticate('local-login', function(err, user, info) {
     if (err) {
     app.errorHandler.handle500(err, res)
     }
     if (!user) {
     res.send(info)
     } else {
     req.logIn(user, function(err) {
     res.send(user)
     })
     }
     })(req, res)
     }
     Controller.signup = function(req, res){
     passport.authenticate('local-signup', function (err, user, info) {

     if(!user)
     res.send(info);
     else
     res.send(user);
     })(req, res);

     }





*/
    return Controller
};