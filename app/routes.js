module.exports = function(app) {


    var addressController=app.controllers.Address;
	var studentController = app.controllers.Student;
	var userController = app.controllers.User;

	// server routes ===========================================================
	// handle things like api calls
	// authentication routes

	// frontend routes =========================================================
	// route to handle all angular requests


	app.post('/createStudent',studentController.createStudent );
	app.get('/student_',studentController.getAllStudents);
	app.delete('/deleteStudent/:id',studentController.deleteStudent);
	app.get('/getStudent/:id',studentController.getStudent);


	app.put('/updateStudent_/:id',studentController.updateStudent);



	app.post('/createAddress_',addressController.createAddress );
	app.get('/address_',addressController.getAllAddresses);
	app.delete('/deleteAddress_/:id',addressController.deleteAddress);
	app.get('/getAddress_/:id',addressController.getAddress);
	app.get('/showAddress_/:id',addressController.showAddress);


	app.put('/updateAddress_',addressController.updateAddress);




	app.post('/signUp_',userController.signUp );
	app.post('/signIn_',userController.signIn);
	app.get('/signedInUser_',userController.signedInUser);
    app.get('/signOut_',userController.signOut);
	app.post('/jwtSignUp',userController.jwtSignUp);
	app.post('/jwtSignIn',userController.jwtSignIn);



	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});





};