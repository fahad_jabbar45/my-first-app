var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
module.exports = function(passport, app) {
    var User = require('mongoose').model('User');

    var generateHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        //return password;
    };
    var validPassword = function(password, storedPassword) {
        return bcrypt.compareSync(password, storedPassword);
    };
    passport.serializeUser(function(user, done) {
        done(null, user.email);
    });
    // used to deserialize the user
    passport.deserializeUser(function(email, done) {
        User.findOne({
            "email": email.toLowerCase()
        }, function(err, user) {
            if (err) {
                return done(err);
            } else {
                done(err, user);
            }
        });
    });
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    }, function(req, email, password, done) { // callback with email and password from our form
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({
            "email": email.toLowerCase()
        }, function(err, user) {
            if (err) {
                throw err;
            } else if (!user) {
                return done(null, false, {
                    msg: "user does not exist"
                });
            } else if (!validPassword(password, user.password)) {
                return done(null, false, {
                    msg: "password is not valid"
                });
            } else {
                return done(null, user);


                //if(user.visitCount) {
                //    User.findByIdAndUpdate(user._id, {visitCount:++user.visitCount}, function (err, updated) {
                //        if (err) {
                //            app.errorHandler.handle500(err, res);
                //        } else {
                //            return done(null, user)
                //        }
                //    });
                //}
                //else{
                //
                //    User.findByIdAndUpdate(user._id, {visitCount:1}, function (err, updated) {
                //        if (err) {
                //            app.errorHandler.handle500(err, res);
                //        } else {
                //            return done(null, user)
                //        }
                //    });
                //}

            }
        });
        //})
    }));
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    }, function(req, email, password, done) {
        var encryotedPass = generateHash(password);
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {
            User.findOne({
                "email": email.toLowerCase()
            }, function(err, user) {
                if (err) {
                    return done(null, false, {
                        msg: "database error while signup"
                    });
                }
                if (!user) {
                    var user = new User({
                        email: email.toLowerCase(),
                        password: encryotedPass,
                        firstName: req.body.firstName,
                        lastName: req.body.lastName

                    });
                    user.save(function(err) {
                        if (err) {
                            return done(null, false, {
                                msg: "database error while signup"
                            });
                        } else {

                            return done(null, user);
                        }
                    });
                } else {
                    //  error
                    return done(null, false, {
                        msg: "Email already used! Try some other email"
                    });
                }
            });
        });
    }));
}