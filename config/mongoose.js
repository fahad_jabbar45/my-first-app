/**
 * Created by NovatoreSolutions on 17/02/16.
 */

module.exports = function(app) {
    var mongoose = require('mongoose');

    mongoose.connect('mongodb://127.0.0.1/DB');


    for (var model in app.models) {
        mongoose.model(app.models[model].name, app.models[model].schema)
    }

}
